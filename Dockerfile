FROM golang:buster as confd
RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install unzip make

ARG CONFD_VERSION=0.16.0
ADD https://github.com/kelseyhightower/confd/archive/v${CONFD_VERSION}.tar.gz /tmp/

RUN mkdir -p /go/src/github.com/kelseyhightower/confd && \
  cd /go/src/github.com/kelseyhightower/confd && \
  tar --strip-components=1 -zxf /tmp/v${CONFD_VERSION}.tar.gz && \
  go install github.com/kelseyhightower/confd && \
  rm -rf /tmp/v${CONFD_VERSION}.tar.gz

FROM tomcat:8-jdk8-openjdk

RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install git rsync

ADD https://api.bitbucket.org/2.0/repositories/bssgbiks/BIKS-Setup/commit/master /info
RUN git clone https://bitbucket.org/bssgbiks/biks-setup.git /opt/biks-setup

VOLUME /opt/biks

COPY --from=confd /go/bin/confd /usr/local/bin/confd
COPY biks-docker-init.sh /opt/biks-setup

RUN mkdir -p /etc/confd/conf.d && \
  mkdir -p /etc/confd/templates && \
  chmod 777 /opt/biks-setup/*.sh

COPY *.toml /etc/confd/conf.d/
COPY *.tmpl /etc/confd/templates/

ENV foxyConfigsDir /opt/biks/cfg
ENV BIKS_VER 1.8.5.19
ENV BIKS_SQLSRV sqlserver
ENV BIKS_SQLINST "null"
ENV BIKS_DB biks
ENV BIKS_SA sa
ENV BIKS_SAPWD Biks4ever
ENV BIKS_USER biks_user_app
ENV BIKS_USERPWD Biks4ever
ENV BIKS_HTTP "http://localhost:8080/BIKS"

ENV CATALINA_OPTS "-Xms512m -Xmx1048m"

CMD ["sh","-c","cd /opt/biks-setup && ./biks-docker-init.sh && cd /opt/biks && catalina.sh run"]
