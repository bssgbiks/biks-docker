#!/bin/bash
confd -onetime -backend env
rsync --ignore-existing -arq /opt/biks-setup/* /opt/biks
cp /opt/biks/app/BIKS.war /usr/local/tomcat/webapps/BIKS.war
cd /opt/biks
if ./update_db.sh; then printf 'ok'; else ./create-restore_db.sh; fi